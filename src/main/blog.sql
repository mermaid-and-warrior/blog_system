/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.15 : Database - blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `blog`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `aid` int(20) NOT NULL AUTO_INCREMENT,
  `aname` varchar(20) NOT NULL,
  `apassword` varchar(20) NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`aid`,`aname`,`apassword`) values (1,'admin','123');

/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `article_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `article_title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `article_content` mediumtext COMMENT '文章内容',
  `article_view_count` int(11) DEFAULT NULL COMMENT '浏览量',
  `article_comment_count` int(11) DEFAULT NULL COMMENT '评论数',
  `article_like_count` int(11) DEFAULT NULL COMMENT '点赞数',
  `article_collection_count` int(11) DEFAULT NULL COMMENT '收藏数',
  `article_status` int(1) DEFAULT NULL COMMENT '1为已发表，0为草稿',
  `article_createtime` datetime DEFAULT NULL COMMENT '发布时间（若为草稿此字段为空）',
  `article_picture` blob COMMENT '文章封面',
  `article_public_status` int(11) DEFAULT NULL COMMENT '文章公开或私密',
  `article_summary` varchar(255) DEFAULT NULL COMMENT '摘要',
  `original_or_reprint` int(1) DEFAULT NULL COMMENT '1为原创,0为转载',
  PRIMARY KEY (`article_id`),
  KEY `uid` (`uid`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `article` */

insert  into `article`(`article_id`,`uid`,`article_title`,`article_content`,`article_view_count`,`article_comment_count`,`article_like_count`,`article_collection_count`,`article_status`,`article_createtime`,`article_picture`,`article_public_status`,`article_summary`,`original_or_reprint`) values (1,17,'这是一个测试1','测试一',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1),(2,17,'这是一个测试2','测试二',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,1),(3,28,'这是一个测试3','测试三',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,0),(4,20,'这是一个测试草稿1','测试草稿一',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0);

/*Table structure for table `article_category` */

DROP TABLE IF EXISTS `article_category`;

CREATE TABLE `article_category` (
  `article_id` bigint(20) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`article_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `article_category_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`),
  CONSTRAINT `article_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `article_category` */

insert  into `article_category`(`article_id`,`category_id`) values (1,1),(4,2),(2,3),(3,3);

/*Table structure for table `article_tag` */

DROP TABLE IF EXISTS `article_tag`;

CREATE TABLE `article_tag` (
  `article_id` bigint(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`tag_id`),
  KEY `tag_id` (`tag_id`),
  CONSTRAINT `article_tag_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`),
  CONSTRAINT `article_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `article_tag` */

insert  into `article_tag`(`article_id`,`tag_id`) values (1,1),(2,1),(1,2),(4,2),(1,15),(3,18),(4,20),(3,21),(4,29),(2,33),(2,36);

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`category_id`,`category_name`) values (1,'前端'),(2,'后端'),(3,'大数据');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`rid`,`roleName`) values (5,'普通用户'),(6,'学生'),(7,'博客委员'),(8,'老师');

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  PRIMARY KEY (`uid`,`rid`),
  KEY `rid` (`rid`),
  CONSTRAINT `role_users_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`),
  CONSTRAINT `role_users_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `role` (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_users` */

insert  into `role_users`(`uid`,`rid`) values (17,5),(17,6),(20,7),(23,7),(28,8);

/*Table structure for table `tag` */

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag_name` (`tag_name`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `tag` */

insert  into `tag`(`tag_id`,`tag_name`) values (4,'C'),(33,'Docker'),(27,'Dubbo'),(28,'EDAS'),(32,'ElasticSearch'),(21,'Hibernate'),(36,'IDEA'),(17,'IO'),(1,'Java'),(20,'JavaWeb'),(34,'Jenkins'),(18,'JSP'),(14,'JVM'),(29,'MongoDB'),(13,'MyBatis'),(10,'MySQL'),(24,'Redis'),(38,'RocketMQ'),(19,'Servlet'),(37,'Shiro'),(25,'SPA'),(11,'Spring'),(31,'SpringBoot'),(26,'SpringCloud'),(12,'SpringMVC'),(22,'SQL'),(35,'Vue'),(5,'操作系统'),(9,'数据库'),(3,'数据结构'),(2,'算法'),(16,'网络编程'),(6,'计算机网络'),(15,'设计模式'),(8,'面试题');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL COMMENT '用户昵称',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别',
  `telephone` varchar(20) NOT NULL COMMENT '手机号',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `biography` varchar(100) DEFAULT NULL COMMENT '个人简介',
  `address` varchar(50) DEFAULT NULL COMMENT '所在地区',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `specialized` varchar(20) DEFAULT NULL COMMENT '专业',
  `semester` varchar(20) DEFAULT NULL COMMENT '学期',
  `logout` varchar(1) DEFAULT 'Y' COMMENT '激活或注销账号',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `telephone` (`telephone`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`uid`,`username`,`password`,`sex`,`telephone`,`email`,`biography`,`address`,`birthday`,`specialized`,`semester`,`logout`) values (17,'long','123','男','12345678901','longzeguo0819@163.com','我是白羊座','重庆',NULL,'大数据',NULL,'Y'),(20,NULL,'123456',NULL,'12345678902','1870261667@qq.com','这是一个测试test',NULL,NULL,NULL,NULL,'Y'),(23,NULL,'123456',NULL,'12345678903','3230830367@qq.com','这是一个测试ok',NULL,NULL,NULL,NULL,'Y'),(28,'zhangsan','123','女','123','123@qq.com','这是一个测试',NULL,NULL,NULL,NULL,'Y');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
