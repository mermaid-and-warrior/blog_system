package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */

/**
 * 博客类型
 */
public class ArticleCategory {
    private Long article_id; //文章id
    private int category_id;//文章类型id

    public ArticleCategory() {
    }

    public ArticleCategory(Long article_id, int category_id) {
        this.article_id = article_id;
        this.category_id = category_id;
    }

    @Override
    public String toString() {
        return "ArticleCategory{" +
                "article_id=" + article_id +
                ", category_id=" + category_id +
                '}';
    }

    public Long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Long article_id) {
        this.article_id = article_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}
