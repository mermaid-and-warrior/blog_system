package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */

/**
 * 用于处理三级分类
 */
public class NM {
    private String n;//表示上级
    private String m;//表示下级

    @Override
    public String toString() {
        return "NM{" +
                "n='" + n + '\'' +
                ", m='" + m + '\'' +
                '}';
    }

    public NM() {
    }

    public NM(String n, String m) {
        this.n = n;
        this.m = m;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }
}
