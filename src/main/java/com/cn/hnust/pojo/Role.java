package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */
public class Role {
    private int rid;//角色id
    private String roleName;//角色名称

    public Role() {
    }

    @Override
    public String toString() {
        return "Role{" +
                "rid=" + rid +
                ", roleName='" + roleName + '\'' +
                '}';
    }

    public Role(int rid, String roleName) {
        this.rid = rid;
        this.roleName = roleName;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
