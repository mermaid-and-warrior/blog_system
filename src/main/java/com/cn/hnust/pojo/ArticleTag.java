package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */
public class ArticleTag {
    private Long article_id; //文章id
    private int tag_id; //标签id

    public ArticleTag() {
    }

    public ArticleTag(Long article_id, int tag_id) {
        this.article_id = article_id;
        this.tag_id = tag_id;
    }

    @Override
    public String toString() {
        return "ArticleTag{" +
                "article_id=" + article_id +
                ", tag_id=" + tag_id +
                '}';
    }

    public Long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Long article_id) {
        this.article_id = article_id;
    }

    public int getTag_id() {
        return tag_id;
    }

    public void setTag_id(int tag_id) {
        this.tag_id = tag_id;
    }
}
