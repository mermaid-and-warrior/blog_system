package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */
public class View {
    private Long article_id;//博客id

    private int vieww;//日榜、周榜、月榜的博客增加的浏览量

    @Override
    public String toString() {
        return "View{" +
                "article_id=" + article_id +
                ", vieww=" + vieww +
                '}';
    }

    public View() {
    }

    public View(Long article_id, int vieww) {
        this.article_id = article_id;
        this.vieww = vieww;
    }

    public Long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Long article_id) {
        this.article_id = article_id;
    }

    public int getVieww() {
        return vieww;
    }

    public void setVieww(int vieww) {
        this.vieww = vieww;
    }
}
