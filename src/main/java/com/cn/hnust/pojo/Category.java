package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */
public class Category {
    private int category_id;
    private int category_pid;
    private String category_name;

    public Category() {
    }

    public Category(int category_id, int category_pid, String category_name) {
        this.category_id = category_id;
        this.category_pid = category_pid;
        this.category_name = category_name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "category_id=" + category_id +
                ", category_pid=" + category_pid +
                ", category_name='" + category_name + '\'' +
                '}';
    }

    public int getCategory_pid() {
        return category_pid;
    }

    public void setCategory_pid(int category_pid) {
        this.category_pid = category_pid;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}
