package com.cn.hnust.pojo;

import java.util.Date;

/**
 * @anther 龙泽国
 */

/**
 * 博客文章
 */
public class Article {
    private Long article_id; //文章id
    private int uid;//用户id
    private String article_title;//文章标题
    private String article_content;//文章内容
    private int article_view_count;//浏览量
    private int article_comment_count;//评论数
    private int article_like_count;//点赞数
    private int article_collection_count;//收藏数
    private int article_status;//1为已发表，0为草稿
    private Date article_createtime;//发布时间（若为草稿此字段为空）
    private Date article_updatetime;//最近修改时间
    private String article_picture;//文章封面
    private int article_public_status;//文章公开或私密
    private String article_summary;//摘要
    private int original_or_reprint;//1为原创,0为转载
    private int day_view_count; //每日统计浏览量
    private int week_view_count;//每周统计浏览量
    private int month_view_count;//每月统计浏览量

    @Override
    public String toString() {
        return "Article{" +
                "article_id=" + article_id +
                ", uid=" + uid +
                ", article_title='" + article_title + '\'' +
                ", article_content='" + article_content + '\'' +
                ", article_view_count=" + article_view_count +
                ", article_comment_count=" + article_comment_count +
                ", article_like_count=" + article_like_count +
                ", article_collection_count=" + article_collection_count +
                ", article_status=" + article_status +
                ", article_createtime=" + article_createtime +
                ", article_updatetime=" + article_updatetime +
                ", article_picture='" + article_picture + '\'' +
                ", article_public_status=" + article_public_status +
                ", article_summary='" + article_summary + '\'' +
                ", original_or_reprint=" + original_or_reprint +
                ", day_view_count=" + day_view_count +
                ", week_view_count=" + week_view_count +
                ", month_view_count=" + month_view_count +
                '}';
    }

    public Article() {
    }

    public Article(Long article_id, int uid, String article_title, String article_content, int article_view_count, int article_comment_count, int article_like_count, int article_collection_count, int article_status, Date article_createtime, Date article_updatetime, String article_picture, int article_public_status, String article_summary, int original_or_reprint, int day_view_count, int week_view_count, int month_view_count) {
        this.article_id = article_id;
        this.uid = uid;
        this.article_title = article_title;
        this.article_content = article_content;
        this.article_view_count = article_view_count;
        this.article_comment_count = article_comment_count;
        this.article_like_count = article_like_count;
        this.article_collection_count = article_collection_count;
        this.article_status = article_status;
        this.article_createtime = article_createtime;
        this.article_updatetime = article_updatetime;
        this.article_picture = article_picture;
        this.article_public_status = article_public_status;
        this.article_summary = article_summary;
        this.original_or_reprint = original_or_reprint;
        this.day_view_count = day_view_count;
        this.week_view_count = week_view_count;
        this.month_view_count = month_view_count;
    }

    public int getWeek_view_count() {
        return week_view_count;
    }

    public void setWeek_view_count(int week_view_count) {
        this.week_view_count = week_view_count;
    }

    public int getDay_view_count() {
        return day_view_count;
    }

    public void setDay_view_count(int day_view_count) {
        this.day_view_count = day_view_count;
    }

    public int getMonth_view_count() {
        return month_view_count;
    }

    public void setMonth_view_count(int month_view_count) {
        this.month_view_count = month_view_count;
    }

    public Date getArticle_updatetime() {
        return article_updatetime;
    }

    public void setArticle_updatetime(Date article_updatetime) {
        this.article_updatetime = article_updatetime;
    }

    public Long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Long article_id) {
        this.article_id = article_id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getArticle_title() {
        return article_title;
    }

    public void setArticle_title(String article_title) {
        this.article_title = article_title;
    }

    public String getArticle_content() {
        return article_content;
    }

    public void setArticle_content(String article_content) {
        this.article_content = article_content;
    }

    public int getArticle_view_count() {
        return article_view_count;
    }

    public void setArticle_view_count(int article_view_count) {
        this.article_view_count = article_view_count;
    }

    public int getArticle_comment_count() {
        return article_comment_count;
    }

    public void setArticle_comment_count(int article_comment_count) {
        this.article_comment_count = article_comment_count;
    }

    public int getArticle_like_count() {
        return article_like_count;
    }

    public void setArticle_like_count(int article_like_count) {
        this.article_like_count = article_like_count;
    }

    public int getArticle_collection_count() {
        return article_collection_count;
    }

    public void setArticle_collection_count(int article_collection_count) {
        this.article_collection_count = article_collection_count;
    }

    public int getArticle_status() {
        return article_status;
    }

    public void setArticle_status(int article_status) {
        this.article_status = article_status;
    }

    public Date getArticle_createtime() {
        return article_createtime;
    }

    public void setArticle_createtime(Date article_createtime) {
        this.article_createtime = article_createtime;
    }

    public String getArticle_picture() {
        return article_picture;
    }

    public void setArticle_picture(String article_picture) {
        this.article_picture = article_picture;
    }

    public int getArticle_public_status() {
        return article_public_status;
    }

    public void setArticle_public_status(int article_public_status) {
        this.article_public_status = article_public_status;
    }


    public String getArticle_summary() {
        return article_summary;
    }

    public void setArticle_summary(String article_summary) {
        this.article_summary = article_summary;
    }

    public int getOriginal_or_reprint() {
        return original_or_reprint;
    }

    public void setOriginal_or_reprint(int original_or_reprint) {
        this.original_or_reprint = original_or_reprint;
    }
}
