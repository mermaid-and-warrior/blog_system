package com.cn.hnust.pojo;

import java.util.Date;

/**
 * @author:ljc
 * @DATE:2022/3/22
 * @NAME:blog_system
 */
public class RealTimeLikes {
    private long article_id; //文章id
    private int like_people_id; //点赞的人id
    private String like_time;  //点赞时间

    public long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(long article_id) {
        this.article_id = article_id;
    }

    public int getLike_people_id() {
        return like_people_id;
    }

    public void setLike_people_id(int like_people_id) {
        this.like_people_id = like_people_id;
    }

    public String getLike_time() {
        return like_time;
    }

    public void setLike_time(String like_time) {
        this.like_time = like_time;
    }
}
