package com.cn.hnust.pojo;

/**
 * @anther 龙泽国
 */
public class RoleUsers {
    private int uid;//用户id
    private int rid;//角色id

    public RoleUsers() {
    }

    public RoleUsers(int uid, int rid) {
        this.uid = uid;
        this.rid = rid;
    }

    @Override
    public String toString() {
        return "RoleUsers{" +
                "uid=" + uid +
                ", rid=" + rid +
                '}';
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }
}
