package com.cn.hnust.dao;

import com.cn.hnust.pojo.Article;
import com.cn.hnust.pojo.ArticleCategory;
import com.cn.hnust.pojo.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @anther 龙泽国
 */
public interface ArticleCategoryMapper {
    /**
     * 保存博客时用户所选择的 类型
     * @param article1
     * @param category1
     */
    @Insert("insert into article_category(article_id,category_id) values(#{article1.article_id},#{category1.category_id})")
    public void saveArticleAndCategory(@Param("article1") Article article1,@Param("category1") Category category1);

    /**
     * 根据博客id 删除对应的类型
     * @param article_id
     */
    @Delete("delete from article_category where article_id=#{article_id}")
    void deleteArticleCategory(@Param("article_id") Long article_id);

    //通过article_id 查询



    @Select("SELECT * FROM category WHERE category_id IN(\n" +
            "\tSELECT category_id FROM article_category WHERE article_id=#{article_id}\n" +
            ")")
    Category findCategoryByArticleId(@Param("article_id") Long article_id);
}
