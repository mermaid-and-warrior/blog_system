package com.cn.hnust.dao;

import org.apache.ibatis.annotations.Select;

public interface MeldalMapper {

    @Select("SELECT COUNT(*) FROM `article` WHERE uid=#{uid}")
    int countByuid(int uid);
}
