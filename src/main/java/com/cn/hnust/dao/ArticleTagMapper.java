package com.cn.hnust.dao;

import com.cn.hnust.pojo.Article;
import com.cn.hnust.pojo.ArticleTag;
import com.cn.hnust.pojo.Tag;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface ArticleTagMapper {
    /**
     * 保存博客时用户所选择的 标签
     * @param article1
     * @param tag1
     */
    @Insert("insert into article_tag(article_id,tag_id) values(#{article1.article_id},#{tag1.tag_id})")
    void saveArticleAndTag(@Param("article1") Article article1,@Param("tag1") Tag tag1);

    /**
     * 通过tag找到含此标签的所有博客id（ArticleTag）
     * @param tag
     * @return
     */
    @Select("select * from article_tag where tag_id=#{tag.tag_id}")
    List<ArticleTag> findArticleTagByTag(@Param("tag") Tag tag);





    /**
     * 根据博客id 删除对应的标签
     * @param article_id
     */
    @Delete("delete from article_tag where article_id=#{article_id}")
    void deleteArticleTag(@Param("article_id") Long article_id);


    @Select("select * from article_tag where article_id=#{article_id}")
    List<ArticleTag> findArticleTagByArticleId(@Param("article_id") Long article_id);
}
