package com.cn.hnust.dao;

import com.cn.hnust.pojo.Tag;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface TagMapper {
    /**
     * 返回标签集合
     * @return
     */
    @Select("select * from tag")
    List<Tag> findAllTag();

    /**
     * 根据tag_name 查询一个Tag对象
     * @param tagName
     * @return
     */
    @Select("select * from tag where tag_name=#{tagName}")
    Tag findByTagName(@Param("tagName") String tagName);

    @Select("select * from tag where tag_id=#{tag_id}")
    Tag findByTagId(@Param("tag_id") int tag_id);
}
