package com.cn.hnust.dao;


import com.cn.hnust.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @anther 龙泽国
 */


public interface UserMapper {
    /**
     * 用户登录
     * @param account
     * @param password
     * @return
     */
    @Select("SELECT o.* FROM (SELECT * FROM `users` WHERE  telephone =#{account}  OR email=#{account} )o  WHERE `password`=#{password}")
    public User login(@Param("account") String account,@Param("password")  String password);

    /**
     * 注册用户
     * @param user
     */
    @Insert("INSERT INTO users(username,password,sex,telephone,email,biography,address,birthday,specialized,semester) VALUES(#{email},#{password},#{sex},#{telephone},#{email},#{biography},#{address},#{birthday},#{specialized},#{semester})")
    public void register(User user);

    /**
     * 通过邮箱查用户
     * @param user
     * @return
     */
    @Select("select * from users where email=#{email}")
    User findByEmail(User user);

    /**
     * 通过uid查用户
     * @param uid
     * @return
     */
    @Select("select * from users where uid=#{uid}")
    User findByUid(Integer uid);




    /**
     * 忘记密码  修改密码
     * @param user
     */
    @Update("update users set password=#{password} where email=#{email}")
    void forgetPassword(User user);

    /**
     * 编辑资料
     * @param user
     * update blog
     */
    @Update("<script>"
            +"update users"
            +"<set>"
            +"<if test='username!=null'>"
            +"username=#{username},"
            +"</if>"
            +"<if test='sex!=null'>"
            +"sex=#{sex},"
            +"</if>"
            +"<if test='biography!=null'>"
            +"biography=#{biography},"
            +"</if>"
            +"<if test='address!=null'>"
            +"address=#{address},"
            +"</if>"
            +"<if test='birthday!=null'>"
            +"birthday=#{birthday},"
            +"</if>"
            +"<if test='specialized!=null'>"
            +"specialized=#{specialized}"
            +"</if>"
            +"</set>"
            +"where uid=#{uid}"
            +"</script>")
    void edit_personal_information(User user);

    /**
     * 通过uid查询用户信息
     * @param user
     * @return
     */
    @Select("select * from users where uid=#{uid}")
    User findByUid1(User user);
}
