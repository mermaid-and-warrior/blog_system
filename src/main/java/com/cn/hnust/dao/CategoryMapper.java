package com.cn.hnust.dao;

import com.cn.hnust.pojo.Category;
import com.cn.hnust.pojo.NM;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface CategoryMapper {
    /**
     * 返回关卡类型集合
     * @return
     */
    @Select("select * from category")
    List<Category> findAllCategory();


    /**
     * 查询二三级分类
     * @return
     */
    @Select("SELECT n.category_name n,m.category_name m FROM category n,category m  WHERE n.category_id = m.category_pid AND n.`category_pid`!=0 ORDER BY n")
    List<NM> findMap23();
    /**
     * 查询一二级分类
     * @return
     */
    @Select("SELECT n.category_name n,m.category_name m FROM category n,category m  WHERE n.category_id = m.category_pid AND n.`category_pid`=0 ORDER BY n")
    List<NM> findMap12();

    /**
     * 根据category_name查询一个Category对象
     * @param category_name
     * @return
     */
    @Select("select * from category where category_name=#{category_name}")
    Category findByCategoryName(@Param("category_name") String category_name);
}
