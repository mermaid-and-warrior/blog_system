package com.cn.hnust.dao;

import com.cn.hnust.pojo.Admin;
import org.apache.ibatis.annotations.Delete;
import com.cn.hnust.pojo.View;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface AdminMapper {
    /**
     * 管理员登录
     * @param aname
     * @param apassword
     * @return
     */
    @Select("select * from admin where aname=#{aname} and apassword=#{apassword}")
    public Admin login(@Param("aname") String aname, @Param("apassword")  String apassword);

    @Delete("")
    void delete();

    /**
     * 更新日榜
     */
    @Update("UPDATE article SET day_view_count=article_view_count")
    void updateDayView();

    /**
     * 前十个日榜
     * @return
     */
    @Select("SELECT article_id,article_view_count-day_view_count vieww FROM article ORDER BY vieww DESC LIMIT 10")
    List<View> selectDayView();

    /**
     * 更周周榜
     */
    @Update("UPDATE article SET week_view_count=article_view_count")
    void updateWeekView();

    /**
     * 前十个周榜
     * @return
     */
    @Select("SELECT article_id,article_view_count-week_view_count vieww FROM article ORDER BY vieww DESC LIMIT 10")
    List<View> selectWeekView();

    /**
     * 更新月榜
     */
    @Update("UPDATE article SET month_view_count=article_view_count")
    void updateMonthView();

    /**
     * 前十个月榜
     * @return
     */
    @Select("SELECT article_id,article_view_count-month_view_count vieww FROM article ORDER BY vieww DESC LIMIT 10")
    List<View> selectMonthView();
}
