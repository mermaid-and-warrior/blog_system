package com.cn.hnust.dao;

import com.cn.hnust.pojo.*;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface ArticleMapper {
    /**
     * 查询个人主页已发布的博客
     * @param user
     * @return
     */
    @Select("select * from article where uid=#{user.uid} and article_status=1")
    public List<Article> findUserArticle(@Param("user") User user);

    /**
     * 查询个人草稿
     * @param user
     * @return
     */
    @Select("select * from article where uid=#{user.uid} and article_status=0")
    public List<Article> findUserDraft(@Param("user") User user);



    /**
     * 保存博客或草稿
     * @param article
     * @param user
     */
    @Insert("insert into article(uid,article_title,article_content" +
            ",article_status,article_createtime,article_updatetime" +
            ",article_picture,article_public_status,article_summary" +
            ",original_or_reprint) " +
            "values(#{user.uid},#{article.article_title},#{article.article_content}" +
            ",#{article.article_status},#{article.article_createtime},#{article.article_updatetime}" +
            ",#{article.article_picture},#{article.article_public_status},#{article.article_summary}" +
            ",#{article.original_or_reprint})")
    void userSaveArticle(@Param("article") Article article,@Param("user") User user);


    /**
     * 通过时间字符串找到Article对象
     * @param format
     * @return
     */
    @Select("SELECT * FROM article WHERE article_updatetime=#{format} and uid=#{uid}")
    Article findArticleByDate(@Param("format") String format,@Param("uid") int uid);


    /**
     * 通过博客id查询博客（展示博客内容）
     * @param article_id
     * @return
     */
    @Select("select * from article where article_id=#{article_id}")
    Article findArticleByArticleId(@Param("article_id") Long article_id);

    /**
     * 通过博客id删除博客
     * @param article_id
     */

    @Delete("delete from article where article_id=#{article_id}")
    void delUserArticle(@Param("article_id") Long article_id);


    /**
     * 浏览量+1
     */
    @Update("UPDATE article SET article_view_count=article_view_count+1 where article_id=#{article_id}")
    void updateArticleViewCount(@Param("article_id") Long article_id);

    /**
     * 编辑博客or草稿
     * @param article
     */
    @Update("update article set article_title=#{article.article_title},article_content=#{article.article_content}," +
            "article_status=#{article.article_status},article_createtime=#{article.article_createtime}," +
            "article_updatetime=#{article.article_updatetime},article_picture=#{article.article_picture}," +
            "article_public_status=#{article.article_public_status},article_summary=#{article.article_summary}," +
            "original_or_reprint=#{article.original_or_reprint}  where article_id=#{article.article_id}")
    void updateArticleOrDraft(@Param("article") Article article);

    /**
     * 删除发表的博客到草稿箱里面
     * @param article_id
     * @param uid
     */
    @Update("UPDATE  `article` SET `article_status`=3 WHERE `article_id`=#{article_id} AND `uid`=#{uid}")
    void deletetorecyclebin(@Param("article_id") String article_id,@Param("uid") String uid);


    /**
     * 增加一次某一篇文章的点赞
     * @param article_id
     */
    @Update("update article set article_like_count=article_like_count+1 where article_id=#{article_id}")
    void addlikeCount(@Param("article_id") long article_id);

    /**
     * 取消一次某一篇文章的点赞
     * @param article_id
     */
    @Update("update article set article_like_count=article_like_count-1 where article_id=#{article_id}")
    void deletelikeCount(@Param("article_id") long article_id);

    /**
     * 添加真实点赞数据
     * @param rlt
     */
    @Insert("insert into real_time_likes(article_id,like_people_id,like_time) values(#{article_id},#{like_people_id},#{like_time})")
    void addRealTimeLike(RealTimeLikes rlt);

    /**
     * 根据文章id查询uid
     * @param article_id
     * @return
     */
    @Select("select article_title from article where article_id=#{article_id}")
    String selectTitleByArticleId(@Param("article_id") long article_id);

    /**
     * 查询是否有点赞记录
     * @param article_id
     * @param uid
     */
    @Select("select * from real_time_likes where article_id=#{article_id} and like_people_id=#{uid}")
    RealTimeLikes selectLikeRecord(@Param("article_id") long article_id,@Param("uid") int uid);

    /**
     * 删除点赞记录
     * @param article_id
     * @param uid
     */
    @Delete("delete from real_time_likes where article_id=#{article_id} and like_people_id=#{uid}")
    void deleteLikeRecord(@Param("article_id") long article_id,@Param("uid") int uid);

    /**
     * 收藏博客时是否已经收藏该博客
     * @param article_id
     * @param uid
     * @return
     */
     @Select("select favorite_id from favorite_article where article_id=#{article_id} and uid=#{uid}")
    Long findByaidAnduid(@Param("article_id") Long article_id,@Param("uid") int uid);

    /**
     * 收藏博客
     * @param article_id
     * @param uid
     * @param format
     */
     @Insert("insert into favorite_article(article_id,uid,favorite_date) values(#{article_id},#{uid},#{format})")
    void addfavoritearticle(@Param("article_id") Long article_id,@Param("uid") int uid,@Param("format") String format);

    /**
     * 取消收藏
     * @param isexistence
     */
    @Delete("delete from favorite_article where favorite_id=#{isexistence}")
    void delfavoritearticle(@Param("isexistence") Long isexistence);
}
