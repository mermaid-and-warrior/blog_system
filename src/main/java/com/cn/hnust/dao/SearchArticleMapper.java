package com.cn.hnust.dao;

import com.cn.hnust.pojo.Article;
import com.cn.hnust.pojo.Tag;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @anther 龙泽国
 */
public interface SearchArticleMapper {

    /**
     * 根据 标题 查询相关博客
     * @param keywords
     * @return
     */
    @Select("SELECT * FROM article WHERE article_title LIKE concat('%',#{keywords},'%')")
    List<Article> searchByTitleKeywords(@Param("keywords") String keywords);

    /**
     * 根据 摘要 查询相关博客
     * @param keywords
     * @return
     */
    @Select("SELECT * FROM article WHERE article_summary LIKE concat('%',#{keywords},'%')")
    List<Article> searchBySummaryKeywords(@Param("keywords") String keywords);
    /**
     * 根据 标题 or 摘要 or 标签 查询相关博客
     * @param keywords
     * @return
     */
    @Select("SELECT * FROM article WHERE article_id IN(\n" +
            "\tSELECT article_id FROM article_tag WHERE tag_id IN (\n" +
            "\tSELECT tag_id FROM tag WHERE tag_name=#{keywords}\n" +
            "\t)\n" +
            ")OR \n" +
            "article_id IN (\n" +
            "SELECT article_id FROM article WHERE article_title LIKE concat('%',#{keywords},'%') OR article_summary LIKE concat('%',#{keywords},'%')\n" +
            ")")
    List<Article> searchByTitleSummaryAndTagKeywords(@Param("keywords") String keywords);

}
