package com.cn.hnust.utils;

import com.alibaba.fastjson.JSON;
import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @anther 龙泽国
 */

/**
 * 检验Token是否存在
 */
public class CheckTokenUtil {
    public static ResultInfo check(HttpServletRequest request, RedisTemplate redisTemplate) {
        String token = null;
        try {
            token = request.getHeader("Authorization");
            HashMap hashMap = JSON.parseObject(token, HashMap.class);
            token = (String) hashMap.get("token");
            if(redisTemplate.opsForValue().get(token)!=null){
                return new ResultInfo(true, redisTemplate.opsForValue().get(token), null);
            }
        } catch (Exception e) {

        }
        return new ResultInfo(false, null, "token已失效！请重新登录！");
    }
}
