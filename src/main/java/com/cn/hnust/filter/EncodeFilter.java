package com.cn.hnust.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @anther 龙泽国
 */
@WebFilter("/*")
public class EncodeFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException,IOException {
        // 接收请求字符集
        servletRequest.setCharacterEncoding("utf-8");
        // 放行
        chain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }

}