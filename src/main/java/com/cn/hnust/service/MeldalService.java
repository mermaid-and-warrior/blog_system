package com.cn.hnust.service;

import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;

public interface MeldalService {
    /**
     * 将所有的徽章的信息发送给客户端
     * @param user
     * @return
     */
    ResultInfo meldaldisplay(User user);
}
