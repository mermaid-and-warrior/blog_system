package com.cn.hnust.service.impl;

import com.cn.hnust.dao.ArticleMapper;
import com.cn.hnust.dao.ArticleTagMapper;
import com.cn.hnust.dao.SearchArticleMapper;
import com.cn.hnust.dao.TagMapper;
import com.cn.hnust.pojo.*;
import com.cn.hnust.service.SearchArticleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @anther 龙泽国
 */
@Service("searchArticleService")
public class SearchArticleServiceImpl implements SearchArticleService {

    @Autowired
    private SearchArticleMapper searchArticleMapper;
    @Autowired
    private TagMapper tagMapper;
    @Autowired
    private ArticleTagMapper articleTagMapper;
    @Autowired
    private ArticleMapper articleMapper;

    /**
     * 根据关键词搜索 博客(根据title、summary、tag查找)
     *
     * @param keywords
     * @return
     */
    public ResultInfo searchByKeywords(Integer pageNum, Integer pageSize, String keywords, User user) {
        ResultInfo resultInfo = new ResultInfo();
        PageInfo<Article> searchPageInfo = null;

        //分页显示
        PageHelper.startPage(pageNum, pageSize);
        List<Article> articleList = searchArticleMapper.searchByTitleSummaryAndTagKeywords(keywords);

        searchPageInfo = new PageInfo<Article>(articleList);

        List list=new ArrayList();
        list.add(searchPageInfo);
        list.add(user);

        resultInfo.setFlag(true);
        resultInfo.setData(list);

        return resultInfo;
    }
}
