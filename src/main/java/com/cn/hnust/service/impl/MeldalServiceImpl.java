package com.cn.hnust.service.impl;

import com.cn.hnust.dao.MeldalMapper;
import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.MeldalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("meldalService")
public class MeldalServiceImpl implements MeldalService {
    @Autowired
    private MeldalMapper meldalMapper;

    /**
     * 提供徽章数据
     * @param user
     * @return
     */
    public ResultInfo meldaldisplay(User user) {
        ResultInfo resultInfo = new ResultInfo();
        int count=meldalMapper.countByuid(user.getUid());
        //这个集合里面装的是所有的徽章的信息，且是分类好的
        ArrayList<Object> arrayList = new ArrayList<Object>();
        //这个集合装的是根据博客的数量进行徽章分类的信息
        ArrayList<Integer> integers = new ArrayList<Integer>();
        integers.add(count);

        //分类好的数据都会放到最大的集合里面
        arrayList.add(integers);

        //如果还有其他的类型的徽章的话将会加判断
        if (count==0){
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("抱歉，您目前还没有徽章，请继续加油哦");
        }else {
            resultInfo.setFlag(true);
            resultInfo.setData(arrayList);
        }
        return resultInfo;
    }
}
