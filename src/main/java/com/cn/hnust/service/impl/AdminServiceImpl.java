package com.cn.hnust.service.impl;

import com.cn.hnust.dao.AdminMapper;
import com.cn.hnust.pojo.Admin;
import com.cn.hnust.pojo.View;
import com.cn.hnust.service.AdminService;
import com.cn.hnust.pojo.ResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @anther 龙泽国
 */

@Service("adminService")
public class AdminServiceImpl implements AdminService {
    public static  List<View> dayViews;
    public static  List<View> weekViews;
    public static  List<View> monthViews;
    @Autowired
    private AdminMapper adminMapper;

    /**
     * 管理员登录
     * @param aname
     * @param apassword
     * @return
     */
    public ResultInfo login(String aname, String apassword) {
        ResultInfo resultInfo=new ResultInfo();
        Admin admin=adminMapper.login(aname,apassword);

        if(admin!=null){
            resultInfo.setFlag(true);
        }else {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("账号或密码错误！");
        }
        resultInfo.setData(admin);
        return resultInfo;
    }

    /**
     * 更新日榜并把日榜存入session
     * @return
     */
    public ResultInfo updateDayView() {
        ResultInfo resultInfo = new ResultInfo();
        try {
            dayViews= adminMapper.selectDayView();
//            session.setMaxInactiveInterval(-1);//永久存储
//            session.setAttribute("day_views",views);

            adminMapper.updateDayView();
            resultInfo.setFlag(true);
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("更新失败!");
        }


        return resultInfo;
    }
    /**
     * 更新日榜并把周榜存入session
     * @return
     */
    public ResultInfo updateWeekView() {
        ResultInfo resultInfo = new ResultInfo();
        try {
            weekViews= adminMapper.selectWeekView();
//            session.setMaxInactiveInterval(-1);//永久存储
//            session.setAttribute("week_views",views);


            adminMapper.updateWeekView();
            resultInfo.setFlag(true);
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("更新失败!");
        }
        return resultInfo;
    }
    /**
     * 更新日榜并把月榜存入session
     * @return
     */
    public ResultInfo updateMonthView() {
        ResultInfo resultInfo = new ResultInfo();
        try {
            monthViews= adminMapper.selectMonthView();
//            session.setMaxInactiveInterval(-1);//永久存储
//            session.setAttribute("month_views",views);

            adminMapper.updateMonthView();
            resultInfo.setFlag(true);
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("更新失败!");
        }


        return resultInfo;
    }

    public ResultInfo delete(int id) {
        adminMapper.delete();
        return null;
    }
}
