package com.cn.hnust.service.impl;

import com.cn.hnust.controller.UserController;
import com.cn.hnust.dao.UserMapper;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.UserService;
import com.cn.hnust.pojo.ResultInfo;
//import com.cn.hnust.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * @anther 龙泽国
 */
@Service("userService")

public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    public ResultInfo login(String account, String password) {
        ResultInfo resultInfo = new ResultInfo();
        User user = userMapper.login(account, password);
        if(user!=null){
            resultInfo.setFlag(true);
        }else {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("账号或密码错误！");
        }
        resultInfo.setData(user);

        return resultInfo;
    }



    public ResultInfo register(User user,String code){
        ResultInfo resultInfo = new ResultInfo();
        try {
            userMapper.register(user);
            resultInfo.setFlag(true);
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("电话或邮箱已被注册！");
        }
        return resultInfo;
    }

    /**
     * 忘记密码
     * @param user
     * @return
     */
    public ResultInfo forgetPassword(User user) {
        ResultInfo resultInfo = new ResultInfo();
        try {
            User u = userMapper.findByEmail(user);
            if (u==null){
                resultInfo.setErrorMsg("该用户不存在");
                resultInfo.setFlag(false);
            }
            else {
                userMapper.forgetPassword(user);
                resultInfo.setErrorMsg("修改密码成功");
                resultInfo.setFlag(true);
            }
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("修改失败");
            e.printStackTrace();
        }
        return resultInfo;
    }

    /**
     * 编辑资料
     * @param user
     * @return
     */
    public ResultInfo edit_personal_information(User user) {
        ResultInfo resultInfo = new ResultInfo();
        try {
            userMapper.edit_personal_information(user);
            User user1=userMapper.findByUid1(user);
            resultInfo.setFlag(true);
            resultInfo.setData(user1);
            resultInfo.setErrorMsg("编辑成功");
        } catch (Exception e) {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("编辑失败");
            e.printStackTrace();
        }
        return resultInfo;
    }
}
