package com.cn.hnust.service;

import com.cn.hnust.pojo.ResultInfo;

import javax.servlet.http.HttpSession;

/**
 * @anther 龙泽国
 */
public interface AdminService {
    /**
     * 管理员登录
     * @param aname
     * @param apassword
     * @return
     */
    public ResultInfo login(String aname,String apassword);

    ResultInfo delete(int id);

    /**
     * 更新日榜
     * @return
     */
    ResultInfo updateDayView();

    /**
     * 更新周榜
     * @return
     */
    ResultInfo updateWeekView();

    /**
     * 更新月榜
     * @return
     */
    ResultInfo updateMonthView();
}
