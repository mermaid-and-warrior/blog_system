package com.cn.hnust.service;

import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @anther 龙泽国
 */
public interface SearchArticleService {
    /**
     * 根据关键词 搜索博客 并分页
     * @param pageNum
     * @param pageSize
     * @param keywords
     * @return
     */
    ResultInfo searchByKeywords(Integer pageNum, Integer pageSize, String keywords, User user);
}
