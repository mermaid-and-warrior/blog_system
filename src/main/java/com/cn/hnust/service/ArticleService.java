package com.cn.hnust.service;

import com.cn.hnust.pojo.Article;
import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import com.github.pagehelper.PageInfo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @anther 龙泽国
 */
public interface ArticleService {
    /**
     * 查询某个用户已发布的所有文章
     * @param pageNum
     * @param pageSize
     * @param user
     * @return
     */
    public ResultInfo findUserArticle(Integer pageNum,
                                          Integer pageSize,
                                          User user);

    /**
     * 从数据库中获取 标签、类型 返回给客户端
     * @return
     */
    ResultInfo userWriteArticle();

    /**
     * 保存博客
     * @param article
     * @param tags
     * @param category
     * @return
     */
    ResultInfo userSaveArticle(Article article, List<String> tags, String category,User user);

    /**
     * 查看博客详情
     * @param article_id
     * @return
     */
    ResultInfo viewArticle(Long article_id);

    /**
     * 查询某个用户的所有草稿
     * @param pageNum
     * @param pageSize
     * @param user
     * @return
     */
    ResultInfo findUserDraft(int pageNum, int pageSize, User user);

    /**
     * 进入编辑博客
     * @param article_id
     * @return
     */
    ResultInfo intoEditArticle(Long article_id);

    /**
     * 编辑并提交博客
     * @param article
     * @param tags
     * @param category_name
     * @return
     */
    ResultInfo saveEditArticle(Article article, List<String> tags, String category_name);

    /**
     * 修改点赞量
     * @param article_id
     * @param uid
     * @return
     */
    ResultInfo likeCount(long article_id, int uid, String time);

    /**
     * 删除博客
     * @param article_id
     */
    ResultInfo userDeleteArticle(Long article_id);


    /**
     * 删除已经发表的博客到草稿箱里面
     * @param article_id
     * @param uid
     * @return
     */
    ResultInfo deletetorecyclebin(String article_id, String uid);



    /**
     * 收藏博客
     * @param article_id
     * @param uid
     * @return
     */
    ResultInfo favoriteArticle(Long article_id, int uid);
}
