package com.cn.hnust.service;


import com.cn.hnust.pojo.User;
import com.cn.hnust.pojo.ResultInfo;

/**
 * @anther 龙泽国
 */
public interface UserService {
    /**
     * 用户登录
     * @param account
     * @param password
     * @return
     */
    public ResultInfo login(String account, String password);

    /**
     * 用户注册
     * @param user
     * @return
     */
    ResultInfo register(User user,String code);

    /**
     * 忘记密码
     * @param user
     * @return
     */
    ResultInfo forgetPassword(User user);

    /**
     * 编辑资料
     * @param user
     * @return
     */
    ResultInfo edit_personal_information(User user);
}
