package com.cn.hnust.controller;

import com.cn.hnust.pojo.User;
import com.cn.hnust.service.AdminService;
import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.service.impl.AdminServiceImpl;

import com.cn.hnust.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @anther 龙泽国
 */

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    /**
     * 管理员登录
     * @param aname
     * @param apassword
     * @return
     */
    @RequestMapping("/login")
    @ResponseBody
    public ResultInfo login(@RequestParam("aname") String aname,
                            @RequestParam("apassword") String apassword){
        return adminService.login(aname,apassword);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResultInfo delete(@RequestParam("id") int id){
        return adminService.delete(id);
    }

    /**
     * 更新日榜
     * @return
     */
    @RequestMapping("/updateDayView")
    @ResponseBody
    public ResultInfo updateDayView(HttpSession session){
        return adminService.updateDayView();
    }
    /**
     * 更新周榜
     * @return
     */
    @RequestMapping("/updateWeekView")
    @ResponseBody
    public ResultInfo updateWeekView(){
        return adminService.updateWeekView();
    }
    /**
     * 更新月榜
     * @return
     */
    @RequestMapping("/updateMonthView")
    @ResponseBody
    public ResultInfo updateMonthView(){
        return adminService.updateMonthView();
    }


    /**
     * 测试方法
     * @param request
     * @return
     */
    @RequestMapping("/test")
    @ResponseBody
    public User test(HttpServletRequest request, @RequestParam("list") List<String> list,@RequestParam("list2") String [] list2) {



//        String token = request.getHeader("token");
//        Boolean token1 = redisTemplate.hasKey("token");
//        Long token2 = redisTemplate.getExpire("token", TimeUnit.SECONDS);
        User token3 = (User) redisTemplate.opsForValue().get(redisTemplate.opsForValue().get("token"));


        for (String s : list) {
            System.out.println(s);
        }
        for (String s : list2) {
            System.out.println(s);
        }

        System.out.println(list);
        System.out.println(Arrays.toString(list2));


        System.out.println("=======================");
        List<Integer> list1=new ArrayList<Integer>();
        list1.add(1);
        list1.add(333);
        list1.add(123);
        System.out.println(list1);
        return token3;
    }

}
