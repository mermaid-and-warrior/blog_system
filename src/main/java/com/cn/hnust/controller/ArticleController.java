package com.cn.hnust.controller;

import com.cn.hnust.pojo.Article;
import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import com.cn.hnust.pojo.View;
import com.cn.hnust.service.ArticleService;
import com.cn.hnust.utils.CheckTokenUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * @anther 龙泽国
 */
@Controller
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    /**
     * 通过分页获取用户已发布的所有博客
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/findUserArticle")
    @ResponseBody
    public ResultInfo findUserArticle(@RequestParam("pageNum") int pageNum,
                                      @RequestParam("pageSize") int pageSize,
                                      HttpServletRequest request) {
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.findUserArticle(pageNum, pageSize,(User) check.getData());
        }else {
            return check;
        }
    }

    /**
     * 通过分页获取用户草稿
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/findUserDraft")
    @ResponseBody
    public ResultInfo findUserDraft(@RequestParam("pageNum") int pageNum,
                                    @RequestParam("pageSize") int pageSize,HttpServletRequest request) {

        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.findUserDraft(pageNum, pageSize, (User)check.getData());
        }else {
            return check;
        }
    }


    /**
     * 用户保存博客
     *
     * @param article
     * @param tags
     * @param category_name
     * @return
     */
    @RequestMapping("/userSaveArticle")
    @ResponseBody
    public ResultInfo userSaveArticle(@RequestBody Article article,
                                      @RequestParam("tags") String[] tags,
                                      @RequestParam("category_name") String[] category_name,
                                      HttpServletRequest request) {
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            List<String> tagss = new ArrayList<String>();
            for (String tag : tags) {
                tagss.add(tag.substring(6));
            }
            return articleService.userSaveArticle(article, tagss, category_name[2], (User)check.getData());
        }else {
            return check;
        }




    }

    /**
     * 用户进入写博客时，从数据库中获取可选类型和标签
     *
     * @return
     */
    @RequestMapping("/userWriteArticle")
    @ResponseBody
    public ResultInfo userWriteArticle(HttpServletRequest request) {

        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.userWriteArticle();
        }else {
            return check;
        }

    }


    /**
     * 查看博客内容详情
     *
     * @param article_id
     * @return
     */
    @RequestMapping("/viewArticle")
    @ResponseBody
    public ResultInfo viewArticle(@RequestParam("article_id") Long article_id,
                                  HttpServletRequest request) {


        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.viewArticle(article_id);
        }else {
            return check;
        }


    }

    /**
     * 进入编辑博客
     * @param article_id
     * @return
     */
    @RequestMapping("/intoEditArticle")
    @ResponseBody
    public ResultInfo editArticle(@RequestParam("article_id") Long article_id,
                                  HttpServletRequest request) {

        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.intoEditArticle(article_id);
        }else {
            return check;
        }


    }

    /**
     * 点赞量
     * @param article_id
     * @param request
     * @return
     */
    @RequestMapping("/likeCount")
    @ResponseBody
    public ResultInfo likeCount(@RequestParam("article_id") long article_id,
                                @RequestParam("time") String time,
                                HttpServletRequest request){
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            User user = (User) check.getData();
            return articleService.likeCount(article_id,user.getUid(),time);
        }
        else {
            return check;
        }
    }


    /**
     * 提交并编辑博客
     * @param article
     * @param tags
     * @param category_name
     * @return
     */
    @RequestMapping("/saveEditArticle")
    @ResponseBody
    public ResultInfo saveArticle(@RequestBody Article article,
                                  @RequestParam("tags") String[] tags,
                                  @RequestParam("category_name") String[] category_name,
                                  HttpServletRequest request) {

        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            List<String> tagss = new ArrayList<String>();
            for (String tag : tags) {
                tagss.add(tag.substring(6));
            }
            return articleService.saveEditArticle(article,tagss,category_name[2]);

        }else {
            return check;
        }


    }

    /**
     * 用户删除博客或草稿箱博客
     * @param article_id
     */
    @RequestMapping("/userDeleteArticle")
    @ResponseBody
    public ResultInfo userDeleteArticle(@RequestParam("article_id") Long article_id,HttpServletRequest request) {
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return articleService.userDeleteArticle(article_id);
        }else {
            return check;
        }
    }

    /**
     * 删除发表的文章到到回收站
     * @param article_id,uid
     * @param uid
     */
    @RequestMapping("/deletetorecyclebin")
    @ResponseBody
    public ResultInfo deletetorecyclebin(@RequestParam("article_id") String article_id,@RequestParam("uid") String uid,HttpServletRequest request) {
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return  articleService.deletetorecyclebin(article_id,uid);
        }else {
            return check;
        }
    }



    /**
     * 用户收藏博客
     * @param article_id
     * @param request
     * @return
     */
    @RequestMapping("/favoriteArticle")
    @ResponseBody
    public ResultInfo favoriteArticle(@RequestParam("article_id") Long article_id,HttpServletRequest request) {
//        (User)check.getData()
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            User data = (User) check.getData();
            return articleService.favoriteArticle(article_id,data.getUid());
        }else {
            return check;
        }
    }
}
