package com.cn.hnust.controller;

import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.MeldalService;
import com.cn.hnust.utils.CheckTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 吴宝生
 */
@Controller
@RequestMapping("/meldal")
public class MeldalController {

    @Autowired
    private MeldalService meldalService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    /**
     * 将所有的徽章的信息传给客户端
     * @param user
     * @param request
     * @return
     */
    @RequestMapping("/meldaldisplay")
    @ResponseBody
    public ResultInfo meldaldisplay(@RequestBody User user, HttpServletRequest request){
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return meldalService.meldaldisplay(user);
        }else {
            return check;
        }

    }
}
