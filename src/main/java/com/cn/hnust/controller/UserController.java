package com.cn.hnust.controller;


import com.cn.hnust.pojo.User;
import com.cn.hnust.service.UserService;
import com.cn.hnust.pojo.ResultInfo;

import com.cn.hnust.utils.CheckTokenUtil;
import com.cn.hnust.utils.JwtUtil;
import com.cn.hnust.utils.MailUtils;
//import com.cn.hnust.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @anther 龙泽国
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     *用户登录
     * @param account
     * @param password
     * @return
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public ResultInfo login(@RequestParam("account") String account,
                            @RequestParam("password") String password) {

        //使用token工具类生成token串(用account生成的)
        String token  = JwtUtil.createToken(account);

        ResultInfo resultInfo = userService.login(account, password);
        User user= (User) resultInfo.getData();
        if (user!=null){
            //设置一天 redis中的用户存活时间
           // redisTemplate.opsForValue().set("token",token,60*60*24,TimeUnit.SECONDS);
            redisTemplate.opsForValue().set(token,user,60*60*24,TimeUnit.SECONDS);
            Map map=new HashMap();
            map.put("token",token);
            map.put("user",user);
            resultInfo.setData(map);
        }
        return resultInfo;
    }

    /**
     * 用户注册
     * @param user
     * @param code
     * @return
     */

    @RequestMapping("/register")
    @ResponseBody
    public ResultInfo register(@RequestBody User user,@RequestParam("code") String code) {
        ResultInfo resultInfo = new ResultInfo();
        if(!code.equalsIgnoreCase((String)redisTemplate.opsForValue().get(user.getEmail()))){
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("验证码错误！");
            return resultInfo;
        }
        return userService.register(user,code);
    }



    /**
     * 发送邮件
     * @param email
     * @return
     * */
    @RequestMapping("/sendEmail")
    @ResponseBody
    public String sendEmail(@RequestParam("email") String email) {
        String code = MailUtils.sendMail(email);
        redisTemplate.opsForValue().set(email,code,300, TimeUnit.SECONDS);
        return code;
    }



    /**
     * 忘记密码
     * @param user
     * @param code
     * @return
     * */
    @RequestMapping("/forgetPassword")
    @ResponseBody
    public ResultInfo forgetPassword(User user,@RequestParam("code") String code) {
        ResultInfo resultInfo = new ResultInfo();
        if(!code.equalsIgnoreCase((String)redisTemplate.opsForValue().get(user.getEmail()))){
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("验证码错误！");
            return resultInfo;
        }
        else {
            redisTemplate.delete(user.getEmail());
            return userService.forgetPassword(user);
        }
    }

    /**
     * 编辑资料
     * @param user
     * @return
     */
    @RequestMapping("/edit_personal_information")
    @ResponseBody
    public ResultInfo edit_personal_information(User user, HttpServletRequest request) {
        String token=null;
        //判断用户是否登录
        ResultInfo resultInfo = CheckTokenUtil.check(request, redisTemplate);
        if (resultInfo.isFlag()){
            resultInfo = userService.edit_personal_information(user);
            User user1= (User) resultInfo.getData();
            if (user1!=null){
                //得到token键
                token = request.getHeader("Authorization");
                //设置一天 redis中的用户存活时间
                redisTemplate.opsForValue().set(token,user1,60*60*24,TimeUnit.SECONDS);
                Map map=new HashMap();
                map.put("token",token);
                map.put("user",user1);
                resultInfo.setData(map);
            }
            return resultInfo;
        }
        else {
            return resultInfo;
        }
    }
}
