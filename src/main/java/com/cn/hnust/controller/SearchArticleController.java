package com.cn.hnust.controller;

/**
 * @anther 龙泽国
 */

import com.cn.hnust.pojo.ResultInfo;
import com.cn.hnust.pojo.User;
import com.cn.hnust.service.SearchArticleService;
import com.cn.hnust.utils.CheckTokenUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 查询博客
 */

@Controller
@RequestMapping("/search")
public class SearchArticleController {
    @Autowired
    private SearchArticleService searchArticleService;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 根据关键词 搜索博客 并分页
     * @param pageNum
     * @param pageSize
     * @param keywords
     * @return
     */
    @RequestMapping("/searchByKeywords")
    @ResponseBody
    public ResultInfo searchByKeywords(@RequestParam("pageNum") int pageNum,
                                       @RequestParam("pageSize") int pageSize,
                                       @RequestParam("keywords") String keywords,
                                       HttpServletRequest request){
        ResultInfo check = CheckTokenUtil.check(request, redisTemplate);
        if(check.isFlag()){
            return searchArticleService.searchByKeywords(pageNum,pageSize,keywords,(User)check.getData());
        }else {
            return check;
        }

    }
}
